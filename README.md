# BEFORE YOU BEGIN

Requires GDK istalled and initialized, edit it and change `myname` from `lkorbasiewicz` to whatever you use. 
All commands will work on a project selected in GDK. 
Add `glab` binary location to your `PATH` and don't forget to `chmod +x` it. 

## USAGE

### glab on

Turns on all instances that names contain `myname` variable value

### glab off

Turns them off

### glab list

Lists all instances containing `myname`

### glab all

List all instances

### sudo glab update

Updates `/etc/hosts` file to map external IP addresses of all instances containing 'myname'
